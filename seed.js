const { PrismaClient } = require('@prisma/client');
const bcrypt = require('bcrypt');
const prisma = new PrismaClient();

async function seedData() {
  try {

    const passwordHash = await bcrypt.hash('P@ssw0rd', 10);
    // Seed data for roles
    const roles = [
      { name: 'admin' },
      { name: 'user' },
    ];

    // Seed data for permissions
    const permissions = [
      { name: 'get' },
      { name: 'post' },
      { name: 'put' },
      { name: 'delete' },
    ];

    // Seed data for rolehaspermission (mapping roles to permissions)
    const roleHasPermissions = [
      { roleId: 1, permissionId: 1 }, // admin has 'get' permission
      { roleId: 1, permissionId: 2 }, // admin has 'post' permission
      { roleId: 1, permissionId: 3 }, // admin has 'put' permission
      { roleId: 1, permissionId: 4 }, // admin has 'delete' permission
      { roleId: 2, permissionId: 1 }, // user has 'get' permission
      { roleId: 2, permissionId: 2 }, // user has 'post' permission
    ];

    // Seed data for users
    const users = [
      {
        email: 'admin_user@mail.com',
        username: 'superadmin',
        name: 'Farhan Riuzaki Super Admin',
        password: passwordHash, // Note: In a real-world application, passwords should be hashed
        roleId: 1, // admin role
      },
      {
        email: 'regular_user@mail.com',
        username: 'regular_user',
        name: 'Farhan Riuzaki User',
        password: passwordHash, // Note: In a real-world application, passwords should be hashed
        roleId: 2, // user role
      },
    ];

    // Clear existing data
    // await prisma.role.deleteMany({});
    // await prisma.permission.deleteMany({});
    // await prisma.roleHasPermission.deleteMany({});
    // await prisma.user.deleteMany({});

    // Seed roles
    const createdRoles = await prisma.role.createMany({ data: roles });

    // Seed permissions
    const createdPermissions = await prisma.permission.createMany({ data: permissions });

    // Seed rolehaspermission
    const createdRoleHasPermissions = await prisma.roleHasPermission.createMany({
      data: roleHasPermissions,
    });

    // Seed users
    const createdUsers = await prisma.user.createMany({ data: users });

    console.log('Seed data inserted successfully.');
  } catch (error) {
    console.error('Error seeding data:', error);
  } finally {
    await prisma.$disconnect();
  }
}

seedData();
