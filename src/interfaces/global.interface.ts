import { Request } from 'express';
// Define a custom interface that extends the original Request interface
export interface CustomRequest extends Request {
  pagination: {
    pageNumber: number;
    pageSize: number;
  };
}

export interface UploadField {
  name: string;
  maxCount: number;
  allowedMimeTypes: string[];
}

export interface PaginationRequest {
  page: number;
  per_page: number;
}

export type HTTPMethod = 'GET' | 'POST' | 'PUT' | 'DELETE';
