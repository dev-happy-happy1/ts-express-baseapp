// Role Interface
export interface Role {
  id: number;
  name: string;
}

// Permission Interface
export interface Permission {
  id: number;
  name: string;
}

// RoleHasPermission Interface
export interface RoleHasPermission {
  id: number;
  roleId: number;
  permissionId: number;
}
