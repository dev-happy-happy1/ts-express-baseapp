import { createLogger, format, transports } from 'winston';
import DailyRotateFile from 'winston-daily-rotate-file';
import { LOG_DIR } from '@config'; // logs dir
import { existsSync, mkdirSync } from 'fs';
import { join } from 'path';

const logDir: string = join(__dirname, LOG_DIR);

if (!existsSync(logDir)) {
  mkdirSync(logDir);
}

const logFormat = format.printf(({ level, message }) => {
  return `${message}`;
});

const customLogger = createLogger({
  format: format.combine(format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss.SSS' }), logFormat),
  transports: [
    new transports.Console(),
    new DailyRotateFile({
      filename: logDir + '/api/request/%DATE%.log',
      level: 'info',
      datePattern: 'YYYY-MM-DD',
    }),
    new DailyRotateFile({
      filename: logDir + '/api/error/%DATE%.log',
      level: 'error',
      datePattern: 'YYYY-MM-DD',
    }),
  ],
});

export default customLogger;
