import { cleanEnv, num, port, str } from 'envalid';

export const ValidateEnv = () => {
  cleanEnv(process.env, {
    NODE_ENV: str(),
    PORT: port(),
    CACHE_PREFIX: str(),
    CACHE_TTL_SECONDS: num(),
    SECRET_KEY: str(),
    LOG_FORMAT: str(),
    LOG_DIR: str(),
    ORIGIN: str(),
    CREDENTIALS: str(),
    REDIS_HOST: str(),
    REDIS_PORT: str(),
    REDIS_EXPIRY: str(),
    REDIS_PASSWORD: str(),
  });
};
