import { IsInt, IsString } from 'class-validator';

// Role DTO
export class RoleDTO {
  @IsInt()
  id: number;

  @IsString()
  name: string;
}

// Permission DTO
export class PermissionDTO {
  @IsInt()
  id: number;

  @IsString()
  name: string;
}

// RoleHasPermission DTO
export class RoleHasPermissionDTO {
  @IsInt()
  id: number;

  @IsInt()
  roleId: number;

  @IsInt()
  permissionId: number;
}
