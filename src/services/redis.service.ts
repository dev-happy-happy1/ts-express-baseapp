import redisClient from '@/client/redis.client';
import { CACHE_PREFIX, CACHE_TTL_SECONDS } from '@config';

// const CACHE_PREFIX = 'my-app:';
// const CACHE_TTL_SECONDS = 3600; // Cache entries expire after 1 hour (adjust as needed).

export async function setCache(key: string, data: any): Promise<void> {
  const serializedData = JSON.stringify(data);
  await redisClient.setex(`${CACHE_PREFIX}${key}`, CACHE_TTL_SECONDS, serializedData);
}

export async function getCache<T>(key: string): Promise<T | null> {
  const data = await redisClient.get(`${CACHE_PREFIX}${key}`);
  return data ? JSON.parse(data) : null;
}

export async function deleteCache(pattern: string): Promise<number> {
  // Fetch all keys matching the pattern
  const keysToDelete: string[] = await redisClient.keys(`${CACHE_PREFIX}${pattern}`);

  // Delete the matched keys
  if (keysToDelete.length > 0) {
    return await redisClient.del(keysToDelete);
  }

  return 0; // No keys deleted
}
