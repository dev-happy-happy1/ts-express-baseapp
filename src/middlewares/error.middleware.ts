import { NextFunction, Request, Response } from 'express';
import { HttpException } from '@exceptions/httpException';
import { logger } from '@utils/logger';

export const ErrorMiddleware = (error: HttpException, req: Request, res: Response, next: NextFunction) => {
  try {
    const status: number = error.status || 500;
    const message: string = error.message || 'Something went wrong';

    // Log the error with the stack trace during development
    if (process.env.NODE_ENV !== 'production') {
      logger.error(`[${req.method}] ${req.path} >> StatusCode:: ${status}, Message:: ${message}\n${error.stack}`);
    } else {
      logger.error(`[${req.method}] ${req.path} >> StatusCode:: ${status}, Message:: ${message}`);
    }

    // Send the error response with the appropriate structure
    res.status(status).json({ status: 'error', statusCode: status, error: true, message });
  } catch (error) {
    // If an error occurs while handling the error, pass it to the next error handler
    next(error);
  }
};
