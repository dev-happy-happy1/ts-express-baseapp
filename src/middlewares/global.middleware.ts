import { CustomRequest, HTTPMethod, UploadField } from '@/interfaces/global.interface';
import { Response, NextFunction, Request } from 'express';
import multer from 'multer';
import path from 'path';
import fs from 'fs';
import Jimp from 'jimp';
import CryptoJS from 'crypto-js';
import rs from 'jsrsasign';
const defaultDestination = 'uploads'; // Default destination folder if not provided
const targetFileSize = 500 * 1024; // 500 KB in bytes
const qualityStep = 5; // Step to decrease the image quality during compression

export const paginationMiddleware = (req: CustomRequest, res: Response, next: NextFunction): any => {
  const pageNumber = parseInt(req.query.pageNumber as string, 10) || 1;
  const pageSize = parseInt(req.query.pageSize as string, 10) || 10;

  if (isNaN(pageNumber) || isNaN(pageSize) || pageNumber <= 0 || pageSize <= 0) {
    return res.status(400).json({ error: 'Invalid pagination parameters.' });
  }

  req.pagination = { pageNumber, pageSize };
  next();
};

// File upload middleware
export const fileUploadMiddleware = (uploadFields: UploadField[], folder?: string) => {
  const destination = folder ? folder : defaultDestination;

  const storage = multer.diskStorage({
    destination: (req: Request, file: Express.Multer.File, cb: Function) => {
      const uploadFolder = path.join(__dirname, `../../public/${destination}`);
      // Check if the destination folder exists, and create it if it doesn't
      fs.mkdir(uploadFolder, { recursive: true }, err => {
        if (err) {
          return cb(err);
        }
        cb(null, uploadFolder);
      });
    },
    filename: (req: Request, file: Express.Multer.File, cb: Function) => {
      const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1e9);
      const originalExtension = path.extname(file.originalname);
      cb(null, file.fieldname + '-' + uniqueSuffix + originalExtension);
    },
  });

  // const upload = multer({ storage }).single('file');
  const upload = multer({ storage }).fields(uploadFields);

  return async (req: Request, res: Response, next: NextFunction) => {
    upload(req, res, async (err: any) => {
      if (err) {
        return res.status(400).json({ error: 'Error uploading file.' });
      }

      // Process uploaded files for each specified field
      for (const field of uploadFields) {
        if (req.files && req.files[field.name]) {
          const uploadedFiles = req.files[field.name];

          for (const uploadedFile of uploadedFiles) {
            if (!field.allowedMimeTypes.includes(uploadedFile.mimetype)) {
              return res.status(400).json({ error: `Invalid MIME type for ${field.name} field` });
            }

            if (uploadedFile.mimetype.startsWith('image/')) {
              // Perform the compression process asynchronously
              if (uploadedFile.size > targetFileSize) {
                compressImage(uploadedFile.path).catch(compressionError => {
                  console.error('Error compressing image:', compressionError);
                });
              }
            }
          }
        }
      }
      next();
    });
  };
};

// Function to compress the image asynchronously
async function compressImage(filePath: string): Promise<void> {
  let quality = 100; // Starting quality

  while (true) {
    const image = await Jimp.read(filePath);
    const compressedImageBuffer = await image.quality(quality).getBufferAsync(Jimp.MIME_JPEG);

    if (compressedImageBuffer.length <= targetFileSize || quality < qualityStep) {
      fs.writeFileSync(filePath, compressedImageBuffer);
      break;
    }

    quality -= qualityStep;
  }
}
export async function deleteFile(filePath: string): Promise<void> {
  return new Promise((resolve, reject) => {
    fs.unlink(filePath, err => {
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
}
