// cacheMiddleware.ts
import { Request, Response, NextFunction } from 'express';
import { getCache, setCache } from '@/services/redis.service';

export async function cacheMiddleware(req: Request, res: Response, next: NextFunction): Promise<void> {
  const cacheKey = req.originalUrl;
  const cachedData = await getCache(cacheKey);

  if (cachedData) {
    res.json(cachedData);
  } else {
    // If the data is not in the cache, proceed to the route handler and store the response in the cache.
    const sendResponse = res.json.bind(res);
    res.json = (data: any) => {
      if (res.statusCode === 200) {
        // Only cache successful responses (status code 200).
        setCache(cacheKey, data).catch(error => {
          console.error('Error caching data:', error);
        });
      }
      return sendResponse(data);
    };
    next();
  }
}
