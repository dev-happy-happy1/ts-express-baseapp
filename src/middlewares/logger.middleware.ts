import { Request, Response, NextFunction } from 'express';
import customLogger from '@utils/customLogger';

// Define a custom type for the response send function
type SendFunction = (body: any) => Response<any, Record<string, any>>;

// Middleware for logging incoming requests
export const requestLoggerMiddleware = (req: Request, res: Response, next: NextFunction) => {
  const logMessage = `INFO|${new Date().toISOString()}||${req.ip}|${req.headers['x-forwarded-for'] || req.connection.remoteAddress}||REQUEST|${
    req.originalUrl
  }||||${req.headers['message_code']}|${req.headers['message_descryption']}||${JSON.stringify(req.body)}`;
  customLogger.info(logMessage);
  next();
};

// Middleware for logging outgoing responses
export const responseLoggerMiddleware = (req: Request, res: Response, next: NextFunction) => {
  const originalSend = res.send as SendFunction;
  const startTime = process.hrtime(); // Record the start time

  res.send = function (body: any) {
    const endTime = process.hrtime(startTime);
    const responseTimeMs = endTime[0] * 1000 + endTime[1] / 1e6; // Calculate response time in milliseconds

    const logMessage = `INFO|${new Date().toISOString()}|${res.statusCode}|${req.ip}|${
      req.headers['x-forwarded-for'] || req.connection.remoteAddress
    }|${responseTimeMs}|RESPONSE|${req.originalUrl}||||${req.headers['message_code']}|${JSON.parse(body).message}||${body}`;
    customLogger.info(logMessage);
    originalSend.call(this, body);
  };
  next();
};

// Middleware for logging errors
export const errorLoggerMiddleware = (error: Error, req: Request, res: Response, next: NextFunction) => {
  const logMessage = `ERROR|${new Date().toISOString()}|${res.statusCode}|${req.ip}|${
    req.headers['x-forwarded-for'] || req.connection.remoteAddress
  }||ERROR|${req.originalUrl}||||${req.headers['message_code']}|${req.headers['message_descryption']}||${error.message}|${error.stack}`;
  customLogger.error(logMessage);
  next(error);
};
