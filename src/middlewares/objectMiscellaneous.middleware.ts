import { Request, Response, NextFunction } from 'express';

export const cdnContentTypeMiddleware = (req: Request, res: Response, next: NextFunction): void => {
  const fileExtension = req.url ? req.url.split('.').pop()?.toLowerCase() : '';
  // console.log(req.url);

  let contentType = 'application/octet-stream';

  if (fileExtension) {
    switch (fileExtension) {
      case 'jpg':
      case 'jpeg':
        contentType = 'image/jpeg';
        break;
      case 'png':
        contentType = 'image/png';
        break;
      case 'gif':
        contentType = 'image/gif';
        break;
      // Add more cases for other file types as needed
    }
  }

  res.set('Content-Type', contentType);
  next();
};
